Class {
	#name : #ExperimentalContract,
	#superclass : #Contract,
	#category : #'Contracts-Tests'
}

{ #category : #'as yet unclassified' }
ExperimentalContract >> after_alwaysFail: result [
	[ result = 'success' ] assert.
]

{ #category : #'as yet unclassified' }
ExperimentalContract >> after_alwaysSucceed: result [
	[ result = 'success' ] assert.
]

{ #category : #'as yet unclassified' }
ExperimentalContract >> after_binary_QuestionQuestion: value returns: result [
	self assert: [ result = 'valid' ] description: 'Expecting string "valid" as the result'.
]

{ #category : #'as yet unclassified' }
ExperimentalContract >> after_returnArg: arg returns: result [
	[ result = 'valid' ] assert
]

{ #category : #'as yet unclassified' }
ExperimentalContract >> always_check [
	[ object isGood ] assert.
]

{ #category : #'as yet unclassified' }
ExperimentalContract >> before_binary_Question: arg [
	[ arg isNotNil ] assert.
]

{ #category : #'as yet unclassified' }
ExperimentalContract >> before_noNills: arg [
	[ arg isNotNil ] assert
]
