For now I'm working on a contract checker. Here is how to use it:

1. You subclass Contract, which is a contract-checking proxy.
2. You add methods for preconditions, postconditions and invariants using a naming convention.
3. You put various asserts (using standard Smalltalk assert mechanisms) into those methods.
4. You intantiate the contract and set it up to relay messages to the target object. `proxy := YourContract new initialize: targetObject.`
5. You use the proxy object in place of the original object and it will check all the conditions.

**Naming Conventions**

For class invariants define method `always_check`.

For method `unary` precondition needs to be named `before_unary` and postcondition - `after_unary:returns:`.

For method `~=` precondition needs to be named `before_TildeEqual` and postcondition - `after_TildeEqual:returns:`. For consistency and because of an additional parameter in postcondition check, I've made conversion method that replaces sybmols with textual names. You can see the conversion table in `Contract class >> initialize`.

For method `some:keywords:` precondition needs to be named `before_some:keywords:` and postcondition - `after_some:keywords:returns:`.

This is obviously not fun to figure out for the first time. I will add a method generator later on. If something like this was used in production it would need to be handled by the IDE.