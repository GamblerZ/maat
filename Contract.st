"
I wrap objects, proxy all messages and check that their methods follow my rules.

First, you subclass me and add methods using the following convention:

For invariants, define method ""always_check"".
For preconditions, define methods ""before_ESCAPEDBINARY"" or ""before_SOME:KEYWORDS:"".
For postconditions, ""after_UNARYNAME:"" or ""after_ESCAPEDBINARY:returns:"" or ""after_SOME:KEYWORDS:returns:"".
For escape names of binary method symbols, take a look at my class' method ""binarySelectorEscapes"".

Each method should use asserts to check for validity of conditions.

After all that, you need to instantiate your newly created class and wrap target object: 

contractor := ContractSubclass new initialize: targetObject.
"
Class {
	#name : #Contract,
	#superclass : #ProtoObject,
	#instVars : [
		'object'
	],
	#classInstVars : [
		'binarySelectorEscapes'
	],
	#category : #Contracts
}

{ #category : #'as yet unclassified' }
Contract class >> check: contract for: object [
	"Invariant check, runs after initialization and after each method"
	(contract class canUnderstand: self invariantSelector)
		ifFalse: [ ^ self ].
	[  
		(Message selector: self invariantSelector)
			sendTo: contract.
	] on: Error do: [ :ex | ContractBreach signal: ex messageText ].
]

{ #category : #accessing }
Contract class >> check: contract for: object after: message returned: result [
	"Postcondition check, runs after methods with matching selectors"
	| postconditionSelector postArguments |
	postconditionSelector := self postconditionSelectorFor: message.
	(contract class canUnderstand: postconditionSelector)
		ifFalse: [ ^ self ].
	postArguments := message arguments, {result}.
	[
		(Message selector: postconditionSelector arguments: postArguments)
			sendTo: contract.
	] on: Error do: [ :ex | ContractBreach signal: ex messageText ].
]

{ #category : #'as yet unclassified' }
Contract class >> check: contract for: object before: message [
	"Precondition check, runs before calling methods with matching selectors"
	| preconditionSelector |
	message selector isUnary
		ifTrue: [^ self ].
	preconditionSelector := self preconditionSelectorFor: message.
	(contract class canUnderstand: preconditionSelector)
		ifFalse: [ ^ self ].
	[ 
		(Message selector: preconditionSelector arguments: message arguments)
			sendTo: contract.
	] on: Error do: [ :ex | ContractBreach signal: ex messageText ].
]

{ #category : #'as yet unclassified' }
Contract class >> escapeBinarySelector: selector [
	"Pre and post conditions always have several parameters, so we need to convert symbols to a keyword"
		
	^ '' join: 
		(selector asArray collect: [ :char | binarySelectorEscapes at: char ]).
]

{ #category : #'as yet unclassified' }
Contract class >> initialize [
	binarySelectorEscapes := { 
	$! -> #Exclamation.
	$% -> #Percent.
	$& -> #Ampersand.
	$* -> #Asterisk.
	$+ -> #Plus.
	$, -> #Comma.
	$/ -> #Slash.
	$< -> #Less.
	$= -> #Equals.
	$> -> #Greater.
	$? -> #Question.
	$@ -> #At.
	$\ -> #Backslash.
	$~ -> #Tilde.
	$- -> #Dash
	} asDictionary.
]

{ #category : #'as yet unclassified' }
Contract class >> invariantSelector [
	^ #always_check.
]

{ #category : #'as yet unclassified' }
Contract class >> postconditionSelectorFor: message [		
	message selector isUnary 
		ifTrue: [ ^ #after_, message selector, ':' ].
	message selector isBinary 
		ifTrue: [ ^ #after_binary_, (self escapeBinarySelector: message selector), ':returns:' ].
	message selector isKeyword
		ifTrue: [ ^ #after_, message selector, 'returns:' ].
	Error signal: 'Invalid message selector'.
]

{ #category : #'as yet unclassified' }
Contract class >> preconditionSelectorFor: message [		
	message selector isUnary 
		ifTrue: [ Error signal: 'Unary messages do not have preconditions' ].
	message selector isBinary 
		ifTrue: [ ^ #before_binary_, (self escapeBinarySelector: message selector), ':' ].
	message selector isKeyword
		 ifTrue: [ ^ #before_, message selector ].
	Error signal: 'Invalid message selector'.
]

{ #category : #'message performing' }
Contract >> assert: aBlock description: aStringOrBlock [
	"Copied from Object, in case condition code needs to concisely assert stuff with messages"
	
	aBlock value 
		ifFalse: [ AssertionFailure signal: aStringOrBlock value ]
]

{ #category : #'reflective operations' }
Contract >> doesNotUnderstand: message [
	| result |
	self class check: self for: object before: message. "precondition"
	result := message sendTo: object.
	self class check: self for: object after: message returned: result. "postcondition"
	self class check: self for: object. "invariant"
	^ result.
]

{ #category : #'message performing' }
Contract >> error: aString [ 
	"Copied from Object, required by perform:withArguments:inSuperclass:"

	^Error new signal: aString
]

{ #category : #'as yet unclassified' }
Contract >> initialize: targetObject [
	object := targetObject. "must assign object first for consistency with other invariant checks"
	self class check: self for: targetObject.
]

{ #category : #'message performing' }
Contract >> perform: selector withArguments: argArray [ 
	"Copied from Object so that we can receive reified messages"
	
	<reflective: #object:performMessageWithArgs:>
	<primitive: 84>
	^ self perform: selector withArguments: argArray inSuperclass: self class
]

{ #category : #'message performing' }
Contract >> perform: selector withArguments: argArray inSuperclass: lookupClass [
	"Copied from Object, required by perform:withArguments:"
	
	<reflective: #object:performMessageInSuperclass:>
	<primitive: 100>
	(selector isSymbol)
		ifFalse: [^ self error: 'selector argument must be a Symbol'].
	(selector numArgs = argArray size)
		ifFalse: [^ self error: 'incorrect number of arguments'].
	(self class == lookupClass or: [self class inheritsFrom: lookupClass])
		ifFalse: [^ self error: 'lookupClass is not in my inheritance chain'].
	self primitiveFailed
]
