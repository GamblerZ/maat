Class {
	#name : #ExperimentalContractor,
	#superclass : #Object,
	#instVars : [
		'isGood'
	],
	#category : #'Contracts-Tests'
}

{ #category : #convenience }
ExperimentalContractor >> ? arg [
	^ arg.
]

{ #category : #convenience }
ExperimentalContractor >> ?? arg [
	^ arg.
]

{ #category : #'as yet unclassified' }
ExperimentalContractor >> alwaysFail [
	^ 'failure'
]

{ #category : #'as yet unclassified' }
ExperimentalContractor >> alwaysSucceed [
	^ 'success'
]

{ #category : #initialization }
ExperimentalContractor >> initialize [ 
	isGood := true.
]

{ #category : #accessing }
ExperimentalContractor >> isGood [
	^ isGood
]

{ #category : #accessing }
ExperimentalContractor >> isGood: value [
	isGood := value.
	^ value.
]

{ #category : #accessing }
ExperimentalContractor >> noNills: arg [
	^ arg
]

{ #category : #accessing }
ExperimentalContractor >> returnArg: arg [
	^ arg
]
