Class {
	#name : #ContractTest,
	#superclass : #TestCase,
	#instVars : [
		'target'
	],
	#category : #'Contracts-Tests'
}

{ #category : #running }
ContractTest >> setUp [
	target := (ExperimentalContract new initialize: ExperimentalContractor new).
]

{ #category : #tests }
ContractTest >> testBinaryPostconditionFailure [
	self 
	should: [ target ?? 'invalid' ]
	raise: ContractBreach.
]

{ #category : #tests }
ContractTest >> testBinaryPostconditionPass [
	self 
	assert: target ?? 'valid'
	equals: 'valid'.
]

{ #category : #tests }
ContractTest >> testBinaryPreconditionFailure [
	self 
	should: [target ? nil]
	raise: ContractBreach.
]

{ #category : #tests }
ContractTest >> testBinaryPreconditionPass [
	self 
	assert: target ? 'something'
	equals: 'something'.
]

{ #category : #tests }
ContractTest >> testInvariantFailure [
	self
	should: [ target isGood: false ]
	raise: ContractBreach.
]

{ #category : #tests }
ContractTest >> testKeywordPostconditionFailure [
	self 
	should: [ target returnArg: 'invalid' ]
	raise: ContractBreach.
]

{ #category : #tests }
ContractTest >> testKeywordPostconditionPass [
	self 
	assert: (target returnArg: 'valid')
	equals: 'valid'.
]

{ #category : #tests }
ContractTest >> testKeywordPredconditionFailure [
	self 
	should: [target noNills: nil ]
	raise: ContractBreach.
]

{ #category : #tests }
ContractTest >> testKeywordPredconditionPass [
	self 
	assert: (target noNills: 'not a nil')
	equals: 'not a nil'.
]

{ #category : #tests }
ContractTest >> testUnaryPostconditionFailure [
	self
	should: [ target alwaysFail ]
	raise: ContractBreach.
]

{ #category : #tests }
ContractTest >> testUnaryPostconditionPass [
	self
	assert: target alwaysSucceed 
	equals:  'success'.
]
