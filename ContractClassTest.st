Class {
	#name : #ContractClassTest,
	#superclass : #TestCase,
	#instVars : [
		'unaryMessageHello',
		'binaryMessagePlus',
		'keywordMessageHello'
	],
	#category : #'Contracts-Tests'
}

{ #category : #initialization }
ContractClassTest >> initialize [
	|literally|
	"comment stating purpose of instance-side message"
	"scope: class-variables  &  instance-variables"	
	literally := MessageCatcher new.
	unaryMessageHello := literally hello.
	binaryMessagePlus  := literally + 1.
	keywordMessageHello := literally hello: 'world'.
]

{ #category : #tests }
ContractClassTest >> testEscapeBinarySelector [
	self 
	assert: (Contract escapeBinarySelector: #+=)
	equals: #PlusEquals
]

{ #category : #tests }
ContractClassTest >> testPostconditionSelectorFor [
	
	self 
	assert: (Contract postconditionSelectorFor: unaryMessageHello)
	equals: #after_hello:.
	
	self 
	assert: (Contract postconditionSelectorFor: binaryMessagePlus)
	equals: #after_binary_Plus:returns:.
	
	self 
	assert: (Contract postconditionSelectorFor: keywordMessageHello)
	equals: #after_hello:returns:.
]

{ #category : #tests }
ContractClassTest >> testPreconditionSelectorFor [
	
	self 
	should: [Contract preconditionSelectorFor:  unaryMessageHello]
	raise: Error.
	
	self 
	assert: (Contract preconditionSelectorFor: binaryMessagePlus)
	equals: #before_binary_Plus:.
	
	self 
	assert: (Contract preconditionSelectorFor: keywordMessageHello)
	equals: #before_hello:.
]
