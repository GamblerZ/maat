"
Exception that happens when there is any kind of exception thrown while checking coniditons or invariants.
"
Class {
	#name : #ContractBreach,
	#superclass : #AssertionFailure,
	#category : #Contracts
}
